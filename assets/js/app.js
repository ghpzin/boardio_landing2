$(document).ready(function() {
    $('pre code').each(function(i, block) {
        hljs.highlightBlock(block);
    });

    $('.open-consulting-popup').on('click', function(e){
        e.preventDefault();

        if($('#consultingModal').length){
            $('#consultingModal').modal();
        } else {
            $('#consultingModalPlaceholder').load('/en/feedback-form', function(){
                $('#consultingModal').modal();
            });
        }
    });

    $('#consultingModalPlaceholder').on('change', '.subject-select', function(){
        if($(this).val() === 'other'){
            $('.other-subject-wrap').show();
        } else {
            $('.other-subject-wrap').hide();
            $('.other-subject-wrap').find('input[type="text"]').val('');
        }
    });
});

function sendConsultRequest(){
    var validateFlag = true;
    var reEmail = /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/;
    var email = $('[name="consultingRequest"] [type="email"]').val();
    var form = $('#consultingModal form');

    $('[name="consultingRequest"] [required]').each(function(){
        validateFlag = validateFlag && !!$(this).val();

        if (!$(this).val()) {
            $(this).siblings('.error-msg').show();
        } else {
            $(this).siblings('.error-msg').hide();
            if (!reEmail.test(email) && !!email) {
                validateFlag = false;
                $('[name="consultingRequest"] .error-email').show();
            } else {
                validateFlag = true;
                $('[name="consultingRequest"] .error-email').hide();
            }
        }
    });

    if (validateFlag) {
        var formData = {
            'name'              : $('#consultingModal [name="name"]').val(),
            'email'             : $('#consultingModal [name="email"]').val(),
            'subject'           : $('#consultingModal [name="subject"]').val(),
            'message'           : $('#consultingModal [name="message"]').val(),
            'optional_subject'  : $('#consultingModal [name="optional_subject"]').val()
        };

        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: formData,
            success: function(data){
                if (data === 'OK'){
                    $('#consultingModal').load('/en/feedback-success', function(){
                        setTimeout(function(){
                            $('#consultingModal').modal('hide');
                        }, 3000);
                    });
                }
            }
        });
    }

    return false;
}