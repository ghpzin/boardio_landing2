var carouselEnable = function(){
	$('#features-carousel').addClass('slide');
	$('#features-carousel').addClass('carousel');
	$('#features-carousel').carousel('cycle');
	$('.carousel-indicators').show();
}

var carouselDisable = function(){
	$('#features-carousel').removeClass('slide');
	$('#features-carousel').removeClass('carousel');	
	$('#features-carousel').carousel('pause');
	$('.carousel-indicators').hide();
}

var mobile = function () {
	return window.matchMedia('only screen and (max-width: 767px), only screen and (max-device-width: 767px)').matches;
}

$(window).on('resize', function () {
	if(mobile()){
		carouselEnable();
	}
	else{
		carouselDisable();
	}
});

$(document).ready(function() {
	$('#features-carousel').swiperight(function() {
		if(mobile()){
			$(this).carousel('prev');
		}
	});
	$('#features-carousel').swipeleft(function() {
		if(mobile()){
			$(this).carousel('next');
		}
	});
});